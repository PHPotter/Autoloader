<?php

namespace PHPotter\Autoloader\SPL;

use \LogicException;

/**
 * Trait SPLAutoloaderTrait
 * 
 * @author Jay Potter
 * @package PHPotter\Autoloader
 * @subpackage Standard PHP Library (SPL) Wrapper
 * 
 */
trait SPLAutoloaderTrait {

    /**
     * @param callable $function
     * @param boolean $prepend
     * @return integer
     * @static
     * @throws LogicException
     */
    final public static function add(callable $function, bool $prepend = FALSE): int {

        /**
         * @static
         * @throws LogicException
         * @var boolean
         */
        $registered = spl_autoload_register($function, TRUE, $prepend);

        if (!$registered) {

            throw new LogicException;
        }

        /** @var array<integer> */
        $indexes = array_keys(self::get());

        return $prepend ?
                reset($indexes) : // (integer) 0
                end($indexes);    // (integer) count($indexes) - 1
    }

    /**
     * @return array
     * @static
     */
    final public static function get(): array {
        /**
         * @static
         * @var array|FALSE
         */
        $functions = spl_autoload_functions();

        if ($functions === FALSE) {

            return [];
        }

        return $functions;
    }

    /**
     * @param integer $index
     * @return boolean
     * @static
     */
    final public static function remove(int $index): bool {

        /** @var array */
        $functions = self::get();

        foreach ($functions as $key => $function) {

            if ($index == $key) {

                /**
                 * @return boolean
                 * @static
                 */
                return spl_autoload_unregister($function);
            }
        }

        return FALSE;
    }

}
