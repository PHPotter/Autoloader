<?php

namespace PHPotter\Autoloader\SPL;

require_once __DIR__ . '/SPLAutoloaderInterface.php';
require_once __DIR__ . '/SPLAutoloaderTrait.php';
require_once __DIR__ . '/../AbstractAutoloader.php';

use \PHPotter\Autoloader\AbstractAutoloader;

/**
 * Abstract AbstractSPLAutoloader
 * 
 * @author Jay Potter
 * @package PHPotter\Autoloader
 * @subpackage Standard PHP Library (SPL) Wrapper
 * 
 */
abstract class AbstractSPLAutoloader extends AbstractAutoloader implements SPLAutoloaderInterface {

    use SPLAutoloaderTrait;

    /**
     * @var integer|NULL
     */
    private $index = NULL;

    /**
     * AbstractSPLAutoloader Constructor
     * 
     * @param boolean $prepend
     * @return void
     */
    public function __construct(bool $prepend = FALSE) {

        $this->register($prepend);

        return;
    }

    /**
     * @abstract
     * @return callable
     */
    abstract public function autoload(): callable;

    /**
     * @final
     * @return void
     */
    final public function register(bool $prepend): void {

        /** @var callable */
        $function = $this->autoload();

        /** @var integer */
        $this->index = self::add($function, $prepend);

        return;
    }

    /**
     * @final
     * @return void
     */
    final public function unregister(): void {

        /** @var boolean */
        $registered = is_integer($this->index);

        if ($registered) {
            self::remove($this->index);
        }

        /** @var NULL */
        $this->index = NULL;

        return;
    }

}
