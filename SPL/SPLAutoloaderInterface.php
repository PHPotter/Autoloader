<?php

namespace PHPotter\Autoloader\SPL;

require_once __DIR__ . '/../AutoloaderInterface.php';

use \PHPotter\Autoloader\AutoloaderInterface;

/**
 * Interface SPLAutoloaderInterface
 * 
 * @author Jay Potter
 * @package PHPotter\Autoloader
 * @subpackage Standard PHP Library (SPL) Wrapper
 */
interface SPLAutoloaderInterface extends AutoloaderInterface {

    /** @return callable */
    public function autoload(): callable;

    /**
     * @param boolean $prepend
     * @return boolean
     */
    public function register(bool $prepend): void;

    /** @return void */
    public function unregister(): void;
}
