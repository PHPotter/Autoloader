<?php

namespace PHPotter\Autoloader;

/**
 * Interface AutoloaderInterface
 * 
 * @author Jay Potter
 * @package PHPotter
 * @subpackage Autoloader
 */
interface AutoloaderInterface {

    /**
     * @param callable $function
     * @return integer
     * @static
     */
    public static function add(callable $function): int;

    /**
     * @return array
     * @static
     */
    public static function get(): array;

    /**
     * @param integer $index
     * @return boolean
     * @static
     */
    public static function remove(int $index): bool;
}