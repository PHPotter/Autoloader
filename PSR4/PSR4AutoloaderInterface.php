<?php

namespace PHPotter\Autoloader\PSR4;

require_once __DIR__ . '/../SPL/SPLAutoloaderInterface.php';

use \PHPotter\Autoloader\SPL\SPLAutoloaderInterface;

/**
 * Interface PSR4AutoloaderInterface
 * 
 * @author Jay Potter
 * @package PHPotter\Autoloader
 * @subpackage PSR4
 */
interface PSR4AutoloaderInterface extends SPLAutoloaderInterface {

    /**
     * @param string $prefix
     * @param string $baseDir
     * @param boolean $prepend
     * @return void
     */
    public function addNamespace(string $prefix, string $baseDir, bool $prepend = FALSE): void;

    /**
     * @param string $class
     * @return mixed The mapped file name on success,
     *               or boolean FALSE on failure
     */
    public function loadClass(string $class);

    /**
     * @param string $prefix
     * @param string $relativeClass
     * @return Boolean FALSE if no mapped file can be loaded, 
     *         or the name of the mapped file that was loaded
     */
    public function loadMappedFile(string $prefix, string $relativeClass);

    /**
     * @param string $file
     * @return boolean
     */
    public function requireFile(string $file): bool;
}
