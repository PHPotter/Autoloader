<?php

namespace PHPotter\Autoloader\PSR4;

/**
 * Trait PSR4AutoloaderTrait
 * 
 * @abstract
 * @author Jay Potter
 * @package PHPotter\Autoloader
 * @subpackage PSR4
 */
trait PSR4AutoloaderTrait {

    /**
     * An associative array where the key is a namespace prefix and the value
     * is an array of base directories for classes in that namespace
     *
     * @var array
     */
    protected $prefixes = [];

    /**
     * Adds a base directory for a namespace prefix
     * 
     * @param string $prefix The namespace prefix
     * @param string $baseDir A base directory for class files in the namespace
     * @param boolean $prepend If TRUE, prepend the base directory to the stack
     *                         instead of appending it; this causes it to be
     *                         searched first rather than last
     * @return void
     */
    public function addNamespace(string $prefix, string $baseDir, bool $prepend = FALSE): void {

        /**
         * normalize namespace prefix
         * 
         * @var string
         */
        $prefix = trim($prefix, '\\') . '\\';

        /**
         * normalize the base directory with a trailing separator
         * 
         * @var string
         */
        $baseDir = rtrim($baseDir, DIRECTORY_SEPARATOR) . '/';

        /** initialize the namespace prefix array */
        if (isset($this->prefixes[$prefix]) === false) {

            $this->prefixes[$prefix] = array();
        }

        /** retain the base directory for the namespace prefix */
        if ($prepend) {

            array_unshift($this->prefixes[$prefix], $baseDir);
        } else {

            array_push($this->prefixes[$prefix], $baseDir);
        }

        return;
    }

    /**
     * Loads the class file for a given class name
     * 
     * @param string $class The fully-qualified class name.
     * @return FALSE|string The mapped file name on success,
     *               or boolean FALSE on failure
     */
    public function loadClass(string $class) {

        /**
         * the current namespace prefix
         * 
         * @var string
         */
        $prefix = $class;

        /**
         * work backwards through the namespace names of the 
         * fully-qualified class name to find a mapped file name
         */
        while (FALSE !== $pos = strrpos($prefix, '\\')) {

            /**
             * retain the trailing namespace separator in the prefix
             * 
             * @var string
             */
            $prefix = substr($class, 0, $pos + 1);

            /**
             * the rest is the relative class name
             * 
             * @var string
             */
            $relativeClass = substr($class, $pos + 1);

            /**
             * try to load a mapped file for the prefix and relative class
             * 
             * @var FALSE|string
             */
            $mappedFile = $this->loadMappedFile($prefix, $relativeClass);

            if ($mappedFile) {

                return $mappedFile;
            }

            /**
             * remove the trailing namespace separator for the next iteration of strrpos()
             * 
             * @var string
             */
            $prefix = rtrim($prefix, '\\');
        }

        /** never found a mapped file */
        return FALSE;
    }

    /**
     * @param string $prefix
     * @param string $relativeClass
     * @return boolean|string FALSE if no mapped file can be loaded, 
     *         or the name of the mapped file that was loaded
     */
    public function loadMappedFile(string $prefix, string $relativeClass) {

        /** are there any base directories for this namespace prefix? */
        if (isset($this->prefixes[$prefix]) === FALSE) {

            return FALSE;
        }

        /** look through base directories for this namespace prefix */
        foreach ($this->prefixes[$prefix] as $baseDir) {

            /**
             * replace the namespace prefix with the base directory,
             * replace namespace separators with directory separators
             * in the relative class name, append with .php
             * 
             * @var string
             */
            $file = $baseDir
                    . str_replace("\\", '/', $relativeClass)
                    . '.php';

            /** if the mapped file exists, require it */
            if ($this->requireFile($file)) {

                /** yes, we're done */
                return $file;
            }
        }

        /** never found it */
        return FALSE;
    }

    /**
     * If a file exists, require it from the file system
     * 
     * @param string $file The file to require
     * @return boolean TRUE if the file exists, FALSE if not
     */
    public function requireFile(string $file): bool {

        if (file_exists($file)) {

            require $file;

            return TRUE;
        }

        return FALSE;
    }

}
