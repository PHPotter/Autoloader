<?php

namespace PHPotter\Autoloader\PSR4;

require_once __DIR__ . '/PSR4AutoloaderInterface.php';
require_once __DIR__ . '/PSR4AutoloaderTrait.php';
require_once __DIR__ . '/../SPL/AbstractSPLAutoloader.php';

use \PHPotter\Autoloader\SPL\AbstractSPLAutoloader;

/**
 * Abstract AbstractPSR4Autoloader
 * 
 * @abstract
 * @author Jay Potter
 * @package PHPotter\Autoloader
 * @subpackage PSR4
 */
abstract class AbstractPSR4Autoloader extends AbstractSPLAutoloader implements PSR4AutoloaderInterface {

    use PSR4AutoloaderTrait;

    /** @return callable */
    public function autoload(): callable {

        return function(string $class) {

            return $this->loadClass($class);
        };
    }

}
