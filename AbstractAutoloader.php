<?php

namespace PHPotter\Autoloader;

require_once __DIR__ . '/AutoloaderInterface.php';

/**
 * Abstract AbstractAutoloader
 * 
 * @abstract
 * @author Jay Potter
 * @package PHPotter
 * @subpackage Autoloader
 */
abstract class AbstractAutoloader implements AutoloaderInterface {

    /**
     * @abstract
     * @param callable $function
     * @return integer
     * @static
     */
    abstract public static function add(callable $function): int;

    /**
     * @abstract
     * @return array
     * @static
     */
    abstract public static function get(): array;

    /**
     * @abstract
     * @param integer $index
     * @return boolean
     * @static
     */
    abstract public static function remove(int $index): bool;
}
